
/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to change the case of an alphabet provided by the user

 *********************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<stdio_ext.h>

//declaring functions used in this file
void changeToLowerCase(char*);
void changeToUpperCase(char*);

int main() {

	//declaring the local variables
	char alphabet,choice;

	//reading the alphabet from user
	printf("enter an alphabet:");
	scanf("%c",&alphabet);
	
	//reading the alphabet if it is invalid
	while(!( (alphabet>='A' && alphabet<='Z') || (alphabet>='a' && alphabet<='z') )) {
		printf("invalid alphabet.Enter valid one:");
		__fpurge(stdin);
		scanf("%c",&alphabet);
	}

	//displaying the entered alphabet
	printf("\n\tentered alphabet is:%c\n",alphabet);
	
	while(1) {
		//displaying the menu of case changing
                printf("__________________________________________________________\n");
		printf("\n1.chage to Lower-case\n2.change to Upper-case\n3.quit\n");
                printf("_____________________________________________________________\n");
		//reading the choice from user
		printf("enter your choice:\n");
		__fpurge(stdin);
		scanf("%c",&choice);
 
		//navigating according to user's choice
		switch(choice) {

			//case to change the upper-case alphabet to lower-case
			case '1':
				if(alphabet>='A' && alphabet<='Z') {
					changeToLowerCase(&alphabet);
					printf("\tafter changing the case the alphabet is:%c\n",alphabet);
				}
				else {
					printf("\tcan't change %c to Lower-case.It is already in Lower-case.\n",alphabet);
				}
				break;

			//case to change the upper-case alphabet to lower-case
			case '2':
				if(alphabet>='a' && alphabet<='z') {
					changeToUpperCase(&alphabet);
					printf("\tafter changing the case the alphabet is:%c\n",alphabet);
				}
				else {
					printf("\tcan't change %c to Upper-case.It is already in Upper-case\n",alphabet);
				}
				break;

			//case to exit from main
			case '3':
				exit(0);

			//default case for invalid choice
			default:
				printf("invalid choice\n");

		}
	}

	//returning from main
	return 0;
}

/*defining changeToLowerCase function which will change the 
given upper-case alphabet to lower-case*/
void changeToLowerCase(char *pointer) {
	*pointer=*pointer|' ';	
	return;
}

/*defining changeToUpperCase function which will change the 
given lower-case alphabet to upper-case*/
void changeToUpperCase(char *pointer) {
	*pointer=*pointer&'_';	
	return;
}

/*************************************************************
enter an alphabet:a

	entered alphabet is:a
__________________________________________________________

1.chage to Lower-case
2.change to Upper-case
3.quit
_____________________________________________________________
enter your choice:
2
	after changing the case the alphabet is:A
__________________________________________________________

1.chage to Lower-case
2.change to Upper-case
3.quit
_____________________________________________________________
enter your choice:
1
	after changing the case the alphabet is:a
__________________________________________________________

1.chage to Lower-case
2.change to Upper-case
3.quit
_____________________________________________________________
enter your choice:
3

*************************************************************/
