/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to check whether the number is even or odd

 *********************************************************************************************/

#include<stdio.h>

int main() {

	//declaring the local variable data
	int data;

	//reading the data from user
	printf("enter the data:");
	scanf("%d",&data);

	//checking and displaying whether it is even or not
	if(data&1) {
		printf("entered number %d is odd\n",data);
	}
	else {
		printf("entered number %d is even\n",data);
	}

	//returninh from main
	return 0;

}

/**********************************************
OUTPUT:
-------
enter the data:10
entered number 10 is even
**********************************************/
