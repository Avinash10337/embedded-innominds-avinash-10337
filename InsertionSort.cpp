/*************************************************************************************************
 
 NAME:                  EMPLOYEE ID:      MOBILE NO:           EMAIL ID:

 V.V.AVINASH            10337             7675057184           v.v.avinash7777@gmail.com



 Purpose
----------
 This program is the implementation of InsertionSort.
   
*************************************************************************************************/



#include <iostream>
using namespace std;
//Creating class InsertionSort which contains constructor and member functions
class InsertionSort {
	int *myarray;
	int size;
	public:
	InsertionSort() {
		cout<<"Enter the size of array"<<endl;
		cin>>size;
		myarray= new int[size];
		cout<<"Enter the elements to be inserted in an array :"<<endl;
		for(int i=0;i<size;i++)
			cin>>myarray[i];
	}
	void displayArrayElements();
	
	void insertionSort();
       //Destructor 
        ~InsertionSort(){
        }
 };

// This function is used to sort the elements in an array
void InsertionSort ::  insertionSort()
{
		int i,j,temp;
		for(i=1;i<size;i++)
		{
			j=i-1;
			temp=myarray[i];
			while(j>=0 && myarray[j]>temp) {
				myarray[j+1]=myarray[j];
				j--;
			}
			myarray[j+1]=temp;
		}
}

// This functon is used to display the elements in an array
void InsertionSort :: displayArrayElements()
{

		for(int i=0;i<size;i++)
			cout<<myarray[i]<<" ";
		cout<<endl;
}

// main method
int main()
{
        // created a object of class InsertionSort  
 	InsertionSort insertionSortObj;
	cout<<"array elements before sorting:"<<endl;
        // calling the method to display array elements
        insertionSortObj.displayArrayElements();
	insertionSortObj.insertionSort();
	cout<<"array elements after sorting:"<<endl;
	insertionSortObj.displayArrayElements();
	return 0;
}


/******************************************************************************

   OUTPUT:
--------------

Enter the size of array
10
Enter the elements to be inserted in an array :
12
456
23
78
34
53
45
2
1
6
array elements before sorting:
12 456 23 78 34 53 45 2 1 6 
array elements after sorting:
1 2 6 12 23 34 45 53 78 456 

********************************************************************************/
