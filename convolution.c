/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to implement convolution of a matrix

 *********************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
// function to print the elements in a matrix
void printMatrix(int rows,int columns,int matrix[][columns]) {

	for(int i=0;i<rows;i++) {
		for(int j=0;j<columns;j++) {
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}
}
int main() {
	int rows,columns;
	int i,j;
        // taking no.of rows from a user
	printf("enter how many rows in a matrix \n");
	scanf("%d",&rows);
	while(rows<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&rows);
	}
        // taking no.of columns from a user
	printf("enter how many columns in a matrix \n");
	scanf("%d",&columns);
	while(columns<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&columns);
	}
	int a[rows][columns];
	int c[rows][columns];
	int b[3][3];

	printf("enter the elements into the matrix\n");
        // entering elements in a matrix
	for(i=0;i<rows;i++) {
		for(j=0;j<columns;j++) {
			scanf("%d",&a[i][j]);
		}
	}
	printf("The elements in the matrix are \n");
	printMatrix(rows,columns,a);
	// entering the elements in a subb matrix
	printf("enter the elements into the sub -matrix\n");
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {
			scanf("%d",&b[i][j]);
		}
	}
        // displaying a elements in a sub matrix
	printf("The elements in the sub-matrix are \n");
	printMatrix(3,3,b);
	printf("\n");

	for(i=0;i<rows;i++) {
		for(j=0;j<columns;j++) {
			c[i][j]=0;
		}
	}
	
	int sum;
	for(i=0;i<rows-2;i++) {
		for(j=0;j<columns-2;j++) {
			sum=0;
			for(int k=0;k<=2;k++) {
				for(int l=0;l<=2;l++) {
				
					sum+=a[i+k][j+l]*b[k][l];
				}
			}
				
				c[i+1][j+1]=sum;
		}
	}
	printf("the matrix after convolution is \n");

	printMatrix(rows,columns,c);
					
        return 0;
}
/****************output*********************
 
enter how many rows in a matrix 
4
enter how many columns in a matrix 
4
enter the elements into the matrix
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
The elements in the matrix are 
1 1 1 1 
1 1 1 1 
1 1 1 1 
1 1 1 1 
enter the elements into the sub -matrix
2
2
2
2
2
2
2
2
2
The elements in the sub-matrix are 
2 2 2 
2 2 2 
2 2 2 

the matrix after convolution is 
0 0 0 0 
0 18 18 0 
0 18 18 0 
0 0 0 0 
*********************************/	
	
