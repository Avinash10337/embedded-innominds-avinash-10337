/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to implement user defined shell.

 *********************************************************************************************/
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
char * arguments[100];


//reading userfrom the stdin
void readComandFromUser() {
	char *string;
	int i=0;
	string=(char *)malloc(20);
	read(0,string,100);
	string[strlen(string)-1]='\0';
	// Returns first token
	char* token = strtok(string, " |");

	while (token != NULL) {
		arguments[i]=token;
		printf("%s\n",arguments[i]);
		i++;
		token = strtok(NULL, " |");
	}
	arguments[i]=token;

}


int main()
{
	int ret = -1,child_exit_status;
	char *userName;
	char cwd[1024];			

	//geting user name
	getlogin_r(userName,10);
	userName[strlen(userName)]='@';
	char name[] = "myShell$ ";
	strcat(userName,name);

	while(1)
	{
		//writing our own username@shellname to the STDOUT
		write(1,userName, strlen(userName));
		readComandFromUser();
		//if entered string equals to PWD
		if(!(strcmp(arguments[0],"pwd"))) {

			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}
		//if entered string equals to Exit
		else if(!(strcmp(arguments[0],"exit"))) {

			exit(0);
		}
		//if entered string equals tp cd
		else if(!(strcmp(arguments[0],"cd"))) {

			chdir(arguments[1]);
			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}


		//if entered string is any linux command 
		ret = fork();
		if(ret == 0) {

			execvp(arguments[0], arguments);
		}
		else
		{
			wait(&child_exit_status);
		}
	}
	return 0;
}
/***********************************output***************************************
vinash@8�6HEVmyShell$ ls -ltr 
ls
-ltr
total 80
-rw-r--r-- 1 avinash avinash   423 Feb  8 12:40  strcpy.c
-rw-r--r-- 1 avinash avinash    82 Feb  8 15:06  mystrrev.c
-rw-r--r-- 1 avinash avinash   673 Feb  8 15:06  mystrcat.c
-rw-r--r-- 1 avinash avinash   557 Feb  8 15:08  MyStrCan.c
drwxr-xr-x 4 avinash avinash  4096 Feb 16 10:32  Reversestringonserver
drwxr-xr-x 4 avinash avinash  4096 Feb 18 13:58 'chatting through socketprogramming'
-rw-rw-r-- 1 avinash avinash  3877 Feb 20 08:52  ChangeAlphabetCase.c
-rw-rw-r-- 1 avinash avinash  1084 Feb 20 08:55  EvenOrOdd.c
-rw-rw-r-- 1 avinash avinash  1366 Feb 20 09:09  PowerOfTwo.c
-rw-r--r-- 1 avinash avinash  2015 Feb 20 09:13  LsCommand.c
-rw-rw-r-- 1 avinash avinash  1075 Feb 20 09:15  MinMaxOfInt.c
-rw-rw-r-- 1 avinash avinash  2287 Feb 20 09:35  BinarySearch.c
-rw-rw-r-- 1 avinash avinash  1282 Feb 20 09:40  Factorial.c
-rw-rw-r-- 1 avinash avinash  1437 Feb 20 09:42  Fibonacci.c
-rw-rw-r-- 1 avinash avinash  1332 Feb 20 09:46  DecimalToBinary.c
-rw-rw-r-- 1 avinash avinash  2222 Feb 22 18:29  myshell.c
-rwxr-xr-x 1 avinash avinash 13128 Feb 22 18:29  a.out
avinash@8�6HEVmyShell$ ls
ls
 a.out		       'chatting through socketprogramming'   Factorial.c   MinMaxOfInt.c   mystrcat.c	   Reversestringonserver
 BinarySearch.c         DecimalToBinary.c		      Fibonacci.c   myshell.c	    mystrrev.c	   strcpy.c
 ChangeAlphabetCase.c   EvenOrOdd.c			      LsCommand.c   MyStrCan.c	    PowerOfTwo.c
avinash@8�6HEVmyShell$ cd ..
cd
..
/home/avinash
avinash@8�6HEVmyShell$ ls
ls
a.out	     calculator   Desktop			    examples.desktop  file4.c	    navna_git	  Public     test1.txt
aspirehyd    calculatorf  Documents			    fib.c	      file.c	    operations.c  strtok1.c  test2.txt
Avinash      check.c	  Downloads			    file2.c	      linkedlist.c  Pictures	  temp	     test3.txt
bharath.csv  csv.csv	  embedded-innominds-avinash-10337  file3.c	      Music	    progs	  Templates  Videos
avinash@8�6HEVmyShell$ pwd
pwd
/home/avinash
avinash@8�6HEVmyShell$ exit
exit


************************************************************************************/

