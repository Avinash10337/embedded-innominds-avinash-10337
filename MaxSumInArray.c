/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to find the maximum sum in an array

 *********************************************************************************************/

#include<stdio.h>
int main() {
	int array[10]={11,2,-3,4,6,7,-4,3,-2,4};
	int j,a=0,temp=0,currentsum=0,sum=0,b=0,count=0;
//this loop is used when there is only 1 positive elemet or 0 positive elemnts  in the array
	for(int i=0;i<10;i++) {
		if(array[i]>0) {
			b=array[i];
			break;
		}
	}


	for(int i=0;i<10;i++) {
		j=i;
		currentsum=0;
		while(array[j]>=0 && j<10) {
			currentsum=currentsum+array[j];
			j++;
			count++;
		}
		if(count==0) {
			a=b;
		}else {
			a=temp+currentsum;
		}
		if(j<9) {
			temp=currentsum+temp+array[j]+array[j+1];
		}

		if(temp>a) {
			i=j+1;
			if(temp>=sum) {
				sum=temp;
			}
		} else {
			i=j;
			if(a>=sum) {
				sum=a;
				temp=0;
			}
		}
	}
	printf("the maximum possible sum in the given array is %d\n",sum);
	return 0;
}
/*************************output************************


the maximum possible sum in the given array is 27
 
  */
