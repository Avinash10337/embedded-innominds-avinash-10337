#include<stdio.h>
#include<stdlib.h>

struct node{
	int rollno;
	char name[10];
	struct node *link;
};

struct node *root = NULL;
// displaying the linked list
void displayList()
{
	struct node *temp;
	temp = root;
	if(temp == NULL)
	{
		printf(" no nodes present in the list\n");
	}
	else
	{
		while(temp!= NULL)
		{
			printf("\n");
			printf("ID: %d\n",temp->rollno);
			printf("NAME:%s\n",temp->name);
			printf("\n");
			printf("_____________________\n");
			temp = temp->link;
		}

	}

}
// adding a node at begining
void addAtBeginning()
{
	struct node *temp;
	temp = (struct node*)malloc(sizeof(struct node));
	printf("enter roll no \n");
	scanf("%d",&temp->rollno);
	printf("enter name: \n");
	scanf("%s",temp->name);
	temp->link = NULL;
	if(root == NULL)
	{
		root = temp;
	}
	else
	{
		temp->link = root;
		root =temp;
	}
}
int main()
{

	int choice;
	// creating a display which takes user choice
	while(1)
	{
		printf("\n##########################################################\n");
		printf("###### SINGLE LINKED LIST OPERATIONS #####################\n");
		printf("##########################################################\n");
		printf("1) AddatBeginning\n");
		printf("2) display List\n");
                printf("------------------------------------------------------\n");
		printf("Enter your choice\n");
		scanf("%d",&choice);
                // using switch case where we call functions
		switch(choice)
		{

			case 1: addAtBeginning();
				break;
			case 2: displayList();
				break;

			default: printf("enter valid choice\n");

		}


	}
 return 0;

}
