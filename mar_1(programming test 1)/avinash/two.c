#include<stdio.h>
#include<stdlib.h>

int setBit(int,int);
int countnoofSetBits(int);
int twosCompliment(int);
int binaryEquivalent(int);

int main()
{
	int choice,data,position = 8;
	printf("enter a number \n");
	scanf("%d",&data);
	printf("#####################################################################\n");
	printf("########### BIT OPERATIONS ##########################################\n");
	printf("#####################################################################\n");
	
        while(1)
	{

	printf("\n1)set 8th bit \n2)count no.of setbits\n3)2's compliment\n4)binary of a number\n");
	printf("enter your choice\n");
	printf("___________________________________________\n");
	__fpurge(stdin);
	scanf("%d",&choice);
	
	switch(choice)    
	{
		case 1 : printf("the result after setting bit is %d\n",(data | (1<<position)));
	                 break;		 
			 

		case 2 : countnoofSetBits(data);
			 break;

		case 3 : printf("the result after two's compliment is %d",data);
			 break;

		case 4 : binaryEquivalent(data);
			 break;

		default : printf("please choose the correct option\n");

	}
	}

	return 0;
}

int binaryEquivalent(int data)
{
	int i;
	for(i=31;i>=0;i--)
	{
		printf(" %d\t",((data>>i)&1));

	}
}




int setBit(int data,int position)
{        
	//printf("before doing the set bit operation: \n");
	//binaryEquivalent(data);
	data = data | (1<<position);
	return data;
}

int countnoofSetBits(int data)
{
	int i,count = 0;
	for(i=31;i>=0;i--)
	{
		if((data>>i)&1)
		{
			count++;
		}
	}
	printf("the number of set bits is : %d\n",count);
}

int twosCompliment(int data)
{
	int i;
	for(i=1;i<31;i++)
	{
		data = data^(1<<i);
	}
	return data;
}
