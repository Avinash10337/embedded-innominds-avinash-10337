
//header files declarartion
#include<stdio.h>
#include<stdlib.h>

int main()
{
	//declaring and initializing two file descriptors with null
	FILE *fp1 = NULL, *fp2 = NULL;
	// checking whether the file is present or not
	if((fp1 = fopen("file1.txt","a+" )) == NULL)
	{ 
		printf("unable to open file1\n");
	}

	else
	{
		printf("file1 opened successfully\n");

	}

	// checking whether the file is present or not
	if((fp2 = fopen("file2.txt","a+")) == NULL)
	{
		printf("unable to open file2\n");
	}
	else
	{
		printf("file2 opened successfully\n");
	}
        
	//declaring variable
	int bottle;
	char buffer[100];
	//using fscanf function,so that it picks the words from the file until end of the file 
	while(bottle = fscanf(fp1,"%s",buffer)!=EOF)
	{      
		// putting the content present in the buffer too file2.txt
		fputs(buffer,fp2);
		//putting the content in file2 by line ny line.so we use fputc
		fputc('\n',fp2);

	}
	// closing the file1 using fclose
	// closing the file2 using fclose
	fclose(fp1);
	fclose(fp2);
	return 0;
} 

