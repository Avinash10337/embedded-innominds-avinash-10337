/*************************************************************************************************
 
 NAME:                  EMPLOYEE ID:      MOBILE NO:           EMAIL ID:

 V.V.AVINASH            10337             7675057184           v.v.avinash7777@gmail.com



 Purpose
----------
 This program is the implementation on queue operations which follows First-In-First_out approach.
 The data which is first added into the queue,will be the first one to delete from the queue. 
 
*************************************************************************************************/


#include<iostream>
#include<stdio.h>
#include<stdlib.h>
using namespace std;

//creating a class QueueImplementation which contains structure and member functions
class QueueImplementation {
  
           struct queue {
		string name;		
		struct queue *next;
	}*front,*rare;
   public:
        // creating a constructor
	QueueImplementation() {
		front=NULL;
		rare=NULL;
	}
   
        // method which is used to add data into the queue
	void enque(string name) {
		struct queue *new_record=new struct queue;
		new_record->name=name;
		if(front==NULL) {
			front=new_record;
			rare=new_record;
			new_record->next=NULL;
			return;
		}
		rare->next=new_record;
		rare=new_record;
		return;
	}
        // method which is used to delete data from the queue
	string deque() {
		string data;
		if(front==NULL) {
			cout<<"Queue is empty"<<endl;
			return NULL;
		}
		struct queue *temp=front;
		data=temp->name;
		front=front->next;
		delete temp;
		temp=NULL;
		return data;
	}
	
       // method which is used to display the data in queue
	void displayQueue() {
		struct queue *temp=front;
		cout<<"\t*****content in queue is :*****"<<endl;
		for(;temp!=NULL;temp=temp->next) {
			cout<<"\t\t"<<temp->name<<endl;
		}
	}

};

int main(void) {
	QueueImplementation queueobj;
	int choice;
	string name;
	while(1) {
                cout<<"---------------------------------------------------------"<<endl;
                cout<<"\t\t\tQueue Operations"<<endl;
		cout<<"*********************MENU**********************"<<endl;
		cout<<"1.ENQUE\n2.DEQUE\n3.Display data present in queue\n4.quit"<<endl;
                cout<<"___________________________________________________________________"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
				cout<<"enter name:"<<endl;
				cin>>name;
				queueobj.enque(name);
				break;
			case 2:
				cout<<"record with data:"<<queueobj.deque()<<" was deleted"<<endl;
				break;
			case 3:
				queueobj.displayQueue();
				break;
			case 4:
				exit(0);
			default:
				cout<<"invalid choice"<<endl;
		}
	} 


}


/*********************************************************************************
        
     OUTPUT
---------------

---------------------------------------------------------
			Queue Operations
*********************MENU**********************
1.ENQUE
2.DEQUE
3.Display data present in queue
4.quit
___________________________________________________________________
enter your choice:
1
enter name:
avinash
---------------------------------------------------------
			Queue Operations
*********************MENU**********************
1.ENQUE
2.DEQUE
3.Display data present in queue
4.quit
___________________________________________________________________
enter your choice:
1
enter name:
innominds
---------------------------------------------------------
			Queue Operations
*********************MENU**********************
1.ENQUE
2.DEQUE
3.Display data present in queue
4.quit
___________________________________________________________________
enter your choice:
3
	*****content in queue is :*****
		avinash
		innominds
---------------------------------------------------------
			Queue Operations
*********************MENU**********************
1.ENQUE
2.DEQUE
3.Display data present in queue
4.quit
___________________________________________________________________
enter your choice:
2
record with data:avinash was deleted
---------------------------------------------------------
			Queue Operations
*********************MENU**********************
1.ENQUE
2.DEQUE
3.Display data present in queue
4.quit
___________________________________________________________________
enter your choice:

***************************************************************************/
 

