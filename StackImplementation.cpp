/**********************************************************************
 
 NAME:                  EMPLOYEE ID:      MOBILE NO:           EMAIL ID:

 V.V.AVINASH            10337             7675057184           v.v.avinash7777@gmail.com



 Purpose
----------
 This program is the implementation on stack operations which follows Last-In-First-Out approach.
 Here the data which is added at the end of stack, is the first one to get deleted.

*********************************************************************/




#include<iostream>
using namespace std;


//   Creating a NODE Structure which contains data and a pointer which points to next
struct stackNode
{
    int data;
    struct stackNode *next;
};

// Creating a class StackImplementation which contains member functions
class StackImplementation
{
    struct stackNode *topOfStack;
    public:
    // creating constructor
    StackImplementation() 
    {
        topOfStack=NULL;
    }
    // method to push data into stack
    void pushDataIntoStack(); 
    // method to delete data from stack

    void popDataFromStack();  
    // method to display the data in the stack
    void displayDataInStack();
};
// PUSH Operation
void StackImplementation::pushDataIntoStack()
{
    int data;
    struct stackNode *ptr;
    cout<<"\nPUSH Operation of stack"<<endl;
    cout<<"Enter data to push into stack: "<<endl;
    cin>>data;
    ptr=new stackNode;
    ptr->data=data;
    ptr->next=NULL;
    if(topOfStack!=NULL)
        ptr->next=topOfStack;
    topOfStack=ptr;
    cout<<"\nNew item is pushed in to the stack!!!";

}

// POP Operation
void StackImplementation::popDataFromStack()
{
    struct stackNode *temp;
    if(topOfStack==NULL)
    {
        cout<<"\nThe stack is empty!!!";
    }
    temp=topOfStack;
    topOfStack=topOfStack->next;
    cout<<"\nPoped data from stack is "<<temp->data;
    delete temp;
}

// Display Stack Elements
void StackImplementation::displayDataInStack()
{
    struct stackNode *ptr1=topOfStack;
    cout<<"\nThe data present in stack is\n";
    while(ptr1!=NULL)
    {
        cout<<ptr1->data<<" ->";
        ptr1=ptr1->next;
    }
    cout<<"NULL\n";
}

// Main function
int main()
{
    StackImplementation stackobj;
    int choice;
    while(1)
    {
        cout<<"---------------------------------------------------"<<endl;
        cout<<"\n\t\tstack operations\n\n";
        cout<<"*********************MENU**********************"<<endl;
        cout<<"1:Push data into stack\n2:Pop data from stack\n3:Display stack data\n4:EXIT\n";
        cout<<"--------------------------------------------------------------------"<<endl;  
        cout<<"enter your choice from menu"<<endl;    
        cin>>choice;
        switch(choice)
        {
            case 1:
                stackobj.pushDataIntoStack();
                break;
            case 2:
                stackobj.popDataFromStack();
                break;
            case 3:
                stackobj.displayDataInStack();
                break;
            case 4:
                return 0;
                break;
            default:
                cout<<"\n Invalid option "<<endl;
                cout<<"enter the options from menu(1-4)"<<endl;
                break;
        }
    }
    return 0;
}



/*********************************************************************
------------------------OUTPUT---------------------------------------
---------------------------------------------------

		stack operations

*********************MENU**********************
1:Push data into stack
2:Pop data from stack
3:Display stack data
4:EXIT
--------------------------------------------------------------------
enter your choice from menu
1

PUSH Operation of stack
Enter data to push into stack: 
1

New item is pushed in to the stack!!!---------------------------------------------------

		stack operations

*********************MENU**********************
1:Push data into stack
2:Pop data from stack
3:Display stack data
4:EXIT
--------------------------------------------------------------------
enter your choice from menu
1

PUSH Operation of stack
Enter data to push into stack: 
2

New item is pushed in to the stack!!!---------------------------------------------------

		stack operations

*********************MENU**********************
1:Push data into stack
2:Pop data from stack
3:Display stack data
4:EXIT
--------------------------------------------------------------------
enter your choice from menu
1

PUSH Operation of stack
Enter data to push into stack: 
3

New item is pushed in to the stack!!!---------------------------------------------------

		stack operations

*********************MENU**********************
1:Push data into stack
2:Pop data from stack
3:Display stack data
4:EXIT
--------------------------------------------------------------------
enter your choice from menu
3

The data present in stack is
3 ->2 ->1 ->NULL
---------------------------------------------------

		stack operations

*********************MENU**********************
1:Push data into stack
2:Pop data from stack
3:Display stack data
4:EXIT
--------------------------------------------------------------------
enter your choice from menu
2

Poped data from stack is 3---------------------------------------------------

		stack operations

*********************MENU**********************
1:Push data into stack
2:Pop data from stack
3:Display stack data
4:EXIT
--------------------------------------------------------------------
enter your choice from menu
3

The data present in stack is
2 ->1 ->NULL
---------------------------------------------------

		stack operations

*********************MENU**********************
1:Push data into stack
2:Pop data from stack
3:Display stack data
4:EXIT
--------------------------------------------------------------------
enter your choice from menu



*/
