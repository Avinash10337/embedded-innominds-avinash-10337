/*----------------------------------------------------------------------------------------------------------------------*/
/*                 NAME:                 EMPLOYEE-ID:           PHONE_NUMBER:                          GMAIL-ID:

              V.V.AVINASH          10337                   7675057184                    v.v.avinash7777@gmail.com
 

  PURPOSE:
 ---------
 This program allows us to enter the details of the student in an university, and provides the display options such as   
 display all students details, display students details by university name, branch .  
    
*-----------------------------------------------------------------------------------------------------------------------*/

#include<iostream>
#include<vector>
#include<stdlib.h>
using namespace std;

//creating university class 
class UniversityStudentInfo {

	//declaring the data members
	string university_name;
	int student_id;
	string student_name;
	double student_contact;
	string student_branch;


public:
	//getters for university name and branch
	string getUniversityName()
	{ 
		return university_name;
	}
	string getBranch() {
		return student_branch;
	}		

	//default constructor for University class
	UniversityStudentInfo() {

		cout<<"enter student RollNo :"<<endl;
		cin>>student_id;
		cout<<"enter student Name :"<<endl;
		cin>>student_name;
		cout<<"enter student ContactNo :"<<endl;
		cin>>student_contact;
		cout<<"enter student Branch(ECE / CSE / MECH / CIVIL) : "<<endl;
		cin>>student_branch;
		cout<<"enter student University Name (SRM / VIT / ANNA): "<<endl;
		cin>>university_name;	
	}


	//displaying students information
	void displayStudentDetails() {

		cout<<"\t\tstudent_RollNo:"<<student_id<<endl;
		cout<<"\t\tstudent name:"<<student_name<<endl;
		cout<<"\t\tstudent contact:"<<student_contact<<endl;
		cout<<"\t\tstudent branch:"<<student_branch<<endl;
		cout<<"\t\tuniversity name:"<<university_name<<endl;
	}
};

//method to search by university-name/branch
void search(vector<UniversityStudentInfo> uni,vector<UniversityStudentInfo>::iterator it,string name) {
	
	int flag=0;

	for (  it = uni.begin(); it != uni.end(); ++it ) {

		if(it->getUniversityName()==name || it->getBranch()==name) {
			flag++;
			cout<<"\t\t*****student-"<<flag<<" details*****"<<endl;
			it->displayStudentDetails();
			cout<<"\t\t-------------------------\n"<<endl;

		}
	}
	if(!flag) {
		cout<<"no student details to display"<<endl;
	}
}

//main function 
int main() {

	//declaring the local variables
	int choice;
	string name;

	//creating vector of unviversity
	vector<UniversityStudentInfo> uni;
	vector<UniversityStudentInfo>::iterator it;
	
	
	while(1) {
		//displaying the menu page
		cout<<"1.Add Student \n2.Display all students\n3.display by university name\n4.display by branch\n5.Quit"<<endl;
		
		//reading the user's choice
		cout<<"enter your choice:"<<endl;
		cin>>choice;

		//navigating according to user's choice		
		switch(choice) {
			case 1:
				{
					UniversityStudentInfo universityObj;
					uni.push_back(universityObj);
				}
				break;
			case 2:
				for (  it = uni.begin(); it != uni.end(); ++it ) {
					cout<<"\t\t*****student details*****"<<endl;
					it->displayStudentDetails();

					cout<<"\t\t-------------------------\n"<<endl;
				}
				break;
			case 3:
				cout<<"enter university name(SRM /VIT / ANNA ) to be search:"<<endl;
				cin>>name;
				search(uni,it,name);
				break;
			case 4:
				cout<<"enter student branch(ECE / CSE /MECH /CIVIL ) to be search:"<<endl;
				cin>>name;
				search(uni,it,name);
				break;
			case 5:
				exit(0);
		}



	}    
	return 0;
}


/**************************************************************************************************************

   OUTPUT:
 -------------

 1.Add Student 
2.Display all students
3.display by university name
4.display by branch
5.Quit
enter your choice:
1
enter student RollNo :
1
enter student Name :
keerthana
enter student ContactNo :
9897678767
enter student Branch(ECE / CSE / MECH / CIVIL) : 
ece
enter student University Name (SRM / VIT / ANNA): 
srm
1.Add Student 
2.Display all students
3.display by university name
4.display by branch
5.Quit
enter your choice:
1
enter student RollNo :
2
enter student Name :
avinash
enter student ContactNo :
76760987654
enter student Branch(ECE / CSE / MECH / CIVIL) : 
cse
enter student University Name (SRM / VIT / ANNA): 
vit
1.Add Student 
2.Display all students
3.display by university name
4.display by branch
5.Quit
enter your choice:
2
		*****student details*****
		student_RollNo:1
		student name:keerthana
		student contact:9897678767
		student branch:ece
		university name:srm
		-------------------------

		*****student details*****
		student_RollNo:2
		student name:avinash
		student contact:7675057184
		student branch:cse
		university name:vit
		-------------------------

1.Add Student 
2.Display all students
3.display by university name
4.display by branch
5.Quit
enter your choice:
3
enter university name(SRM /VIT / ANNA ) to be search:
vit
		*****student-1 details*****
		student_RollNo:2
		student name:avinash
		student contact:7675057184
		student branch:cse
		university name:vit
		-------------------------

1.Add Student 
2.Display all students
3.display by university name
4.display by branch
5.Quit
enter your choice:
4
enter student branch(ECE / CSE /MECH /CIVIL ) to be search:
ece
		*****student-1 details*****
		student_RollNo:1
		student name:keerthana
		student contact:9897678767
                student branch:ece
		university name:srm
		-------------------------

1.Add Student 
2.Display all students
3.display by university name
4.display by branch
5.Quit
enter your choice

***************************************************************************************************************/
