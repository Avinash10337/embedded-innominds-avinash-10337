#include"Server.h"
// function to print the error msg
void error(char *msg){
	perror(msg);
	exit(1);
}

int main(int argc, char **argv){
        // declaring of variables
	int sockfd,newSockfd,portNum;
	char buf[1024];
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	socklen_t cliLen;
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
        // creatio of socket
	sockfd = socket(AF_INET,SOCK_STREAM,0);
        // checking whether the socket is available or not
	if(sockfd<0)error("Error openeing socket");
	memset((char*) &serv_addr,0,sizeof(serv_addr));
        // converting port no from ascii to integer
	portNum = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
	listen(sockfd,5);
	cliLen = sizeof(cli_addr);
	while(1){

		int ret = -1;
		ret = fork();
		if(ret == 0) {
		memset(buf,0,sizeof(buf));
		n = read(newSockfd,buf,1024);
		if(n<0)
			error("ERROR ON READING");
		printf("client= %s\n",buf);
		n = write(newSockfd,buf,strlen(buf));
		if(n<0)
			error("ERROR ON WRITING");
		} else {

			newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
			if(newSockfd<0)
				error("ERROR IN ACCEPTING");
		}	


	}
        // closing newSockfd
	close(newSockfd);
	close(sockfd);
	return 0;
}
