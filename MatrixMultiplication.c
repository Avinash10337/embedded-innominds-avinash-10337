/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to implement the multiplication of two matrices.

 *********************************************************************************************/


#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

// function to display 
void printMatrix(int rows,int columns,int matrix[][columns]) {

	for(int i=0;i<rows;i++){
		for(int j=0;j<columns;j++){
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}
}

int main() {

	int row,column,sum,row1,column1;

        // taking number of rows from user in matrix 1
	printf("enter how many rows in a matrix1 \n");
	scanf("%d",&row);
	while(row<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&row);
	}
        // taking number of columns from user in matrix1
	printf("enter how many columns in a matrix1 \n");
	scanf("%d",&column);
	while(column<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&column);
	}
        

        // taking number of rows from user in matrix2 
	printf("enter how many rows in a matrix2 \n");
	scanf("%d",&row1);
	while(row1<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&row1);
	}
        

        // taking number of rows from user in matrix //
	printf("enter how many columns in a matrix2 \n");
	scanf("%d",&column1);
	while(column1<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&column1);
	}


	if(column!=row1) {

		printf("matrix multiplicaton is not possible \n");
		exit(0);
	}

	srand(getpid());
	int matrix[row][column];
	int matrix1[row1][column1];
	int resultMatrix[row][column1];

	for(int i=0;i<row;i++){
		for(int j=0;j<column;j++){
			matrix[i][j]=rand()%10;
		}
	}
	printf("printing the first matrix\n");

	printMatrix(row,column,matrix);

	for(int i=0;i<row1;i++){
		for(int j=0;j<column1;j++){
			matrix1[i][j]=rand()%10;
		}
	}
	printf("printing the second matrix\n");

	printMatrix(row1,column1,matrix1);

	for(int i=0;i<row;i++){
		for(int j=0;j<column1;j++){
			sum=0;
			for(int k=0;k<column;k++){
				sum+=matrix[i][k]*matrix1[k][j];
			}
			resultMatrix[i][j]=sum;
		}
	}

	printf("printing the resultant matrix after multiplication\n");
	printMatrix(row,column1,resultMatrix);

	return 0;	
}

/*******************************************OUTPUT ************************************
  
  enter how many rows in a matrix1 
3
enter how many columns in a matrix1 
3
enter how many rows in a matrix2 
3
enter how many columns in a matrix2 
3
printing the first matrix
7 7 3 
5 6 8 
5 0 9 
printing the second matrix
9 8 1 
3 8 6 
2 2 5 
printing the resultant matrix after multiplication
90 118 64 
79 104 81 
63 58 50 
 
 **********************/
