#include"client.h"

int n;
int sockfd,portNum;
char buf[1024];
char buff[4]={'b','y','e','\n'};
void error(char *msg){
	perror(msg);
	exit(1);
}
// function in which client reads the data from the server
void * clientRead(void* arg) {
	while(1)
	{
		memset(buf,0,sizeof(buf));
		n = read(sockfd,buf,1024);
		if(n<0)
			error("ERROR ON READING");
		if(!(strcmp(buf,buff))) {

			printf("server=%s\n",buf);
			exit(1);
		}
		else
			printf("server=%s\n",buf);
	}
	return NULL;
}

// // function in which client writes the data to the server
void * clientWrite(void* arg) {
	while(1){
		memset(buf,0,sizeof(buf));
		fgets(buf,1024,stdin);
		n = write(sockfd,buf,strlen(buf));
		if(n<0)
			error("ERROR ON WRITING");
		if(!(strcmp(buf,buff))) 
			exit(1);
	}
	return NULL;
}

// main function which uses the concept of pthread
int main(int argc, char **argv){
	struct hostent *server;
	struct sockaddr_in serv_addr;
	if(argc<3){
		fprintf(stderr,"usage %s hostname port\n",argv[0]);
		exit(0);
	}
	portNum = atoi(argv[2]);
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	server = gethostbyname(argv[1]);
	if(server == NULL){
		fprintf(stderr,"error,no such host\n");
		exit(0);
	}
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	strncpy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portNum);
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
		error("ERROR IN CONNECTING");
	close(sockfd);
	return 0;
}
