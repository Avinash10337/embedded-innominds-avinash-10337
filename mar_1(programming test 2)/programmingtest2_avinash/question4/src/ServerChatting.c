// inclusion of header files
#include"server.h"
int sockfd,newSockfd,portNum;
char buf[1024];
int n;
char buff[4]={'b','y','e','\n'};
// function to display error msg
void error(char *msg){
	perror(msg);
	exit(1);
}

// function to read from the client
void * serverRead(void* arg) {
	while(1){
		memset(buf,0,sizeof(buf));
		n = read(newSockfd,buf,sizeof(buf));
		if(n<0)
			error("ERROR ON READING");
	// comparing whether BYE is present in buf or not,if present then exit
	if(!(strcmp(buf,buff))) {

			printf("client=%s\n",buf);
			exit(0);
		}
		else 
			printf("client=%s\n",buf);
	}
	return NULL;
}

// function to write to client
void * serverWrite(void* arg) {
	while(1)
	{
		memset(buf,0,sizeof(buf));
		n = read(0,buf,1024);
		if(n<0)
			error("ERROR ON READING");
		n = write(newSockfd,buf,strlen(buf));
		if(n<0)
			error("ERROR ON WRITING");
		if(!(strcmp(buf,buff)))
			exit(0);
	}
	return NULL;
}
// main function which uses the concept of pthread 
int main(int argc, char **argv){
	int FILE *fp1,*fp2;

	struct sockaddr_in serv_addr, cli_addr;
	socklen_t cliLen;
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	portNum = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
	listen(sockfd,5);
	cliLen = sizeof(cli_addr);
	newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
	if(newSockfd<0)
		error("ERROR IN ACCEPTING");


	close(sockfd);
	close(newSockfd);
	return 0;
}
