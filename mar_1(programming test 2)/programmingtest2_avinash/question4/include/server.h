// header files for server

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include <pthread.h>

// function prototypes for server
void error(char *);
void * serverRead(void *);
void * serverWrite(void *);
~                              
