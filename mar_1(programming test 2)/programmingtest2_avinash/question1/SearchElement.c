// inclusion of header files
#include<stdio.h>
#include<stdlib.h>

//function which  search the element in the given array 
int binarysearch(int low,int high,int *array,int data) {

	int mid;

	if(low>high)
		return -1;
	// calculating the mid value ,if the given element is less than mid then search from o to mid else from mid+1 to last
	mid=(low+high)/2;
        // checking if the given element is equal or mot in the list of elements
	if(array[mid]==data)
		return mid+1;

	else if(array[mid]>data) {
		mid = mid-1;
		binarysearch(low,mid,array,data);
	}
	else {
		mid = mid+1;
		binarysearch(mid,high,array,data);
	}
      
}
	

// main function which dislays the list of array elements ans asks the user to search the desired element in the list ,and displays the index and the element
int main() {
	// initialising the array
	int array[]={3,5,7,9,10,14,16,35,67,89,90};
	// declaring the variables
	int size,data,index;
	
        printf("the elements in the array are: 3,5,7,9,10,14,16,35,67,89,90\n");
	printf("____________________________________________________\n");
	printf("enter the data to be searched\n");
	scanf("%d",&data);

	index=binarysearch(0,10,array,data);
        // if element is found then displays index and element
	if(index>=0)
		printf("the data  %d is found at index  %d  in the given array \n",data,index);
	else
		// if element not found then displays the msg
		printf("the data %d  is not found in the given array \n",data);
	return 0;


}
