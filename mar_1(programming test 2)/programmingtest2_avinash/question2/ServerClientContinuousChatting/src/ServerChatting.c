
// addingg header files from server.h which was present in include directory
#include"server.h"

// declaring the variables
int sockfd,newSockfd,portNum;
char buf[1024];
int n;
// initializing a char buff of size 4
char buff[4]={'b','y','e','\n'};

// function to display error msg
void error(char *msg){
	perror(msg);
	exit(1);
}

// function to read from the client using read system call
void * serverRead(void* arg) {
	while(1){
		// using memset to set the buf content to 0
		memset(buf,0,sizeof(buf));
		// using read call to read from the client and put it into the buf
		n = read(newSockfd,buf,sizeof(buf));
		if(n<0)
			error("ERROR ON READING");
		// comparing whether BYE is present in buf or not,if present then exit
		if(!(strcmp(buf,buff))) {

			printf("client=%s\n",buf);
			exit(0);
		}
		else 
			printf("client=%s\n",buf);
	}
	return NULL;
}

// function to write to client using write system call
void * serverWrite(void* arg) {
	while(1)
	{       
		// using memset to make zeros in the buf
		memset(buf,0,sizeof(buf));
		// reading the content from the buf
		n = read(0,buf,1024);
		// checing whether the content is present in the buf or not present then printing the error msg
		if(n<0)
			error("ERROR ON READING");
		// using write call to write into the client
		n = write(newSockfd,buf,strlen(buf));
		// checking if the content is present or not ,  if not present then printing error on writing
		if(n<0)
			error("ERROR ON WRITING");

		if(!(strcmp(buf,buff)))
			exit(0);
	}
	return NULL;
}
// main function which uses the concept of pthread and socket programming to make the chat application betweenclient and server and also the  terminating condition
int main(int argc, char **argv){
	// declaring a struct sockaddr_in and its member
	struct sockaddr_in serv_addr, cli_addr;
	// declaring and creating the pthread_t ids
	pthread_t thread_id,thread_id1;
	socklen_t cliLen;
	// checking for the correct number of arguments
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
	// creating a socket with family ,type,port no
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	// checking if the socket is present or not,if not printing msg
	if(sockfd<0)error("Error openeing socket");
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	// converting port numbers from ascii to integers
	portNum = atoi(argv[1]);
	// initialising socket members
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	// binding the socket using bind system call
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
	// using listen system call to listen to the clients
	listen(sockfd,5);
	cliLen = sizeof(cli_addr);
	// accepting the client using accept system call
	newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
	// if not acceptinf then displaying the msg
	if(newSockfd<0)
		error("ERROR IN ACCEPTING");

	// creating a thread using pthread_create and pasiing the serverWrite function
	pthread_create(&thread_id,NULL,serverWrite , NULL);
	// creating a thread using pthread_create and passing the serverRead function
	pthread_create(&thread_id1,NULL,serverRead, NULL);

	// waiting untill the thread completes using pthread_join
	pthread_join(thread_id, NULL);
	// waiting until the thread completes using pthread_join
	pthread_join(thread_id1, NULL);

	// closing the file descriptors
	close(newSockfd);
	close(sockfd);
	return 0;
}
