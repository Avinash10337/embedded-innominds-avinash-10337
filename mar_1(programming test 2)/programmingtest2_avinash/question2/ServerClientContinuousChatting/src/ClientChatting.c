
// inclusion of header files from Client.h from include directory
#include"client.h"

// declaring the variables
int n;
int sockfd,portNum;
char buf[1024];
// initialising the buffer of size[4]
char buff[4]={'b','y','e','\n'};
// function which displays the error msg
void error(char *msg){
	perror(msg);
	exit(1);
}

// function in which client reads the data from the server using read system call
void * clientRead(void* arg) {
	while(1)
	{      
		// using memset to set the buffer values to zero
		memset(buf,0,sizeof(buf));
		// reading from the socket file descriptor to buffer
		n = read(sockfd,buf,1024);
		if(n<0)
			error("ERROR ON READING");
		// comparing whether the both strings are same,if same exit otherwise continuing chatting
		if(!(strcmp(buf,buff))) {

			printf("server=%s\n",buf);
			exit(1);
		}
		else
			printf("server=%s\n",buf);
	}
	return NULL;
}

// // function in which client writes the data to the server
void * clientWrite(void* arg) {
	while(1){
		memset(buf,0,sizeof(buf));
		fgets(buf,1024,stdin);
		// function to write to server using write system call
		n = write(sockfd,buf,strlen(buf));
		if(n<0)
			error("ERROR ON WRITING");
		// comparing the strings ,if same then exit otherwise continue
		if(!(strcmp(buf,buff))) 
			exit(1);
	}
	return NULL;
}

// main function which uses the concept of pthread and socket programming to chat between server and client
int main(int argc, char **argv){
	struct hostent *server;
	pthread_t thread_id,thread_id1;
	struct sockaddr_in serv_addr;
	// checking for the correct number of arguments
	if(argc<3){
		fprintf(stderr,"usage %s hostname port\n",argv[0]);
		exit(0);
	}
	portNum = atoi(argv[2]);
	// creating of socket using socket system call
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	server = gethostbyname(argv[1]);
	if(server == NULL){
		fprintf(stderr,"error,no such host\n");
		exit(0);
	}
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	// initialising he structure family members
	serv_addr.sin_family = AF_INET;
	strncpy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portNum);
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
		error("ERROR IN CONNECTING");
        // creating threads
	pthread_create(&thread_id,NULL,clientRead , NULL);
	pthread_create(&thread_id1,NULL,clientWrite, NULL);
        // waiting until the completion
	pthread_join(thread_id, NULL);
	pthread_join(thread_id1, NULL);
	close(sockfd);
	return 0;
}






