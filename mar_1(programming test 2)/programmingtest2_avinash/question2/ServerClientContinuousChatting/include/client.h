
// header files for client
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<pthread.h>

// function prototypes for client
void * clientRead(void *);
void * clientWrite(void *);
void error(char *);

