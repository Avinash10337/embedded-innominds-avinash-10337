/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to convert the decimal number given by the user to binary eqivalent of that number.

 *********************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
//it converts given decimal number in  binary number and returns the value  
int decimalToBinary(int num) {
	if(num==0) 
		return 0;
	else 
		return (num%2+10 * (decimalToBinary(num/2)));

}

int main() {
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	int a=decimalToBinary(num);
	printf("the binary  number is %d  for the given decimal number  %d\n",a,num);
	return 0;
}
/************************OUTPUT****************************************
enter the number
28
the binary  number is 11100  for the given decimal number  28

enter the number
-8
the binary  number is -1000  for the given decimal number  -8


************************************************************************/
