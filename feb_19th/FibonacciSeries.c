/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to find the fibonacci series of a given number by the user using recirsion.

 *********************************************************************************************/
#include<stdio.h> 
//this function finds the fibonacci series for the given number
int Fibo(int n)
{
	if ( n == 0 )
		return 0;
	else if ( n == 1 )
		return 1;
	else
		return ( Fibo(n-1) + Fibo(n-2) );
} 

int main () 
{ 

	int num,i=0;
	printf("enter the number to find the fibonacci series :\n");
	scanf("%d",&num);
	while(num<0) {
		printf("the number should be greater than 0 \n");
		scanf("%d",&num);
	}

	printf("Fibonacci series for the given number is \n");

	for (int j = 1 ; j <= num ; j++ )
	{
		printf("%d ", Fibo(i));
		i++; 
	}
	printf("\n");

	return 0;
}

/*********************************OUTPUT*************************************

enter the number to find the fibonacci series :
10
Fibonacci series for the given number is 
0 1 1 2 3 5 8 13 21 34 
*******************************************************************************/
