/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to search the element of an array using binary search tree concept.

 *********************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

//it search the element in the given array
int binarysearch(int low,int high,int *array,int data) {

	int mid;

	if(low>high)
		return -1;
	mid=(low+high)/2;

	if(array[mid]==data)
		return mid;

	else if(array[mid]>data) {
		high = mid-1;
		binarysearch(low,high,array,data);
	}
	else {
		low=mid+1;
		binarysearch(low,high,array,data);
	}

}
//it sorts array in ascending order
int * sorting(int *array,int size) {

	int i,j,temp;

	for(i=0;i<size;i++) {
		for(j=0;j<size-1;j++) {
			if(array[j] > array[j+1]) {
				temp = array[j];
				array[j]=array[j+1];
				array[j+1]=temp;
			}
		}
	
	}

	return array;
}
int main() {
	int array[100];
	int size,i,data,index;
	printf("enter the size of array\n");
	scanf("%d",&size);
	while(size<=0) {
		printf("size cannot be negative.....re enter\n");
		scanf("%d",&size);
	}
	printf("enter the elements into array \n");
	for(i=0;i<size;i++) {
		scanf("%d",&array[i]);
	}

	sorting(array,size);

//the elemts in the array after the sorting are 
	for(i=0;i<size;i++) {
		printf("%d\t",array[i]);
	}
	printf("\n");

	printf("enter the data to be searched\n");
	scanf("%d",&data);

	index=binarysearch(0,size-1,array,data);

	if(index>=0)
		printf("the data  %d is found at index  %d  in the given array \n",data,index);
	else
		printf("the data %d  is not found in the given array \n",data);
	return 0;


}

/*********************************OUTPUT*************************************

enter the size of array
4
enter the elements into array 
1
2
5
7
1	2	5	7	
enter the data to be searched
5
the data  5 is found at index  2  in the given array 

*****************************************************************************/
