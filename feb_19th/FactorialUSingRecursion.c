/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to find the factorial of given number using recursion

 *********************************************************************************************/
#include<stdio.h>
//this function returns the factorial of given number
 int fact(int num) {

	 if(num==0 || num==1) 
		 return 1;
	 else
		return (num*fact(num-1));
 }
int main() {
	int num;
	printf("enter the number  :\n");
	scanf("%d",&num);
	while(num<0) {
		printf("the provided number should not be negative \n");
		scanf("%d",&num);
	}
	int a=fact(num);
	printf("the factorial for the given number %d is: %d\n",num,a);
	return 0;
}
/*****************************OUTPUT********************************

enter the number  :
4
the factorial for the given number 4 is: 24

enter the number  :
-9
the provided number should not be negative 


*********************************************************************/
