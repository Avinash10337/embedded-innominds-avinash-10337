
/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program of server which waits for client

 *********************************************************************************************/

#include"Server.h"

void error(char *msg){
	perror(msg);
	exit(1);
}

int main(int argc, char **argv){
	int sockfd,newSockfd,portNum;
	char buf[1024];
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	socklen_t cliLen;
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	portNum = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
	listen(sockfd,5);
	cliLen = sizeof(cli_addr);
	newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
	if(newSockfd<0)error("ERROR IN ACCEPTING");
       while(1){
	       memset(buf,0,sizeof(buf));
	       n = read(newSockfd,buf,1024);
	       if(n<0)
		       error("ERROR ON READING");
	       printf("client= %s\n",buf);
	       memset(buf,0,sizeof(buf));
	       fgets(buf,1024,stdin);
	       n = write(newSockfd,buf,strlen(buf));
	       if(n<0)
		       error("ERROR ON WRITING");
	       int i = strncmp("bye",buf,3);
		       if(i==0)
			       break;
       }
       close(newSockfd);
       close(sockfd);
return 0;
}
