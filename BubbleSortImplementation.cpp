/*************************************************************************************************
 
 NAME:                  EMPLOYEE ID:      MOBILE NO:           EMAIL ID:

 V.V.AVINASH            10337             7675057184           v.v.avinash7777@gmail.com



 Purpose
----------
 This program is the implementation of BubbleSort.
   
*************************************************************************************************/


#include<iostream>
using namespace std;

// creating a class which contains constructor and member functions
class BubbleSortImplementation{

        int *array;
        int size;
        public:
                BubbleSortImplementation(){
                        cout<<"\t\t\tBUBBLE SORT"<<endl;
                        cout<<"_________________________________________"<<endl;
                        cout<<"enter the size of array"<<endl;
                        cin>>size;
                        array = new int[size];
                        cout<<"enter array elements:"<<endl;
                        for(int i=0;i<size;i++)
                        cin>>array[i];
                        }

                void bubbleSort();
                void displayArrayElements();
               // Destructor
                ~BubbleSortImplementation(){
                }
};

// method to sort the elementts in an array by using bubble sort
void BubbleSortImplementation :: bubbleSort(){
	int temp;
	for(int i=0;i<size-1;i++){
		for(int j=0;j<size-i-1;j++){
			if(array[j]>array[j+1]){
				temp=array[j+1];
				array[j+1]=array[j];
				array[j]=temp;
			}
		}
	}
}

// method to display array elements
void BubbleSortImplementation :: displayArrayElements(){

	
        for(int i=0;i<size;i++){
        cout<<array[i]<<" ";
	}

}

// main 
int main(){
	
	BubbleSortImplementation bubbleSort;
	cout<<"Array elements Before sorting:"<<endl;
	bubbleSort.displayArrayElements();
	bubbleSort.bubbleSort();
	cout<<endl<<"Array elements After sorting:"<<endl;	
	bubbleSort.displayArrayElements();
	return 0;
}





/*****************************************************************************************

  OUTPUT:
-----------
 			BUBBLE SORT
_________________________________________
enter the size of array
4
enter array elements:
243
45
334
12
Array elements Before sorting:
243 45 334 12 
Array elements After sorting:
12 45 243 334 


*******************************************************************************************/
