/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to implement the user defined ls command.

 *********************************************************************************************/

#include<stdio.h>
#include<dirent.h>
#include<stdlib.h>
#include<string.h>

int main(int argc,char* argv[])
{
	//declaring the local variables
	DIR *dp;
	struct dirent *ptr;
	
	//checking whether the path is provided or not
	//if not opening the current directory
	if(argc==1) {
		dp=opendir("./");
	} 
	//else opening the given directory
	else { 
		dp=opendir(argv[1]);
	}
	
	//checking whether the given path is exist or not
	//if not printing the corresponding message
	if(dp==NULL) {
		printf("ls: cannot access 'desktop': No such file or directory\n");
		exit(0);
	}

	//otherwise printing the list of the files present in that directory
	while(ptr=readdir(dp))
		if(ptr->d_name[0]!='.')
			printf("%s   ",ptr->d_name);
	printf("\n");

	//returning from main
	return 0;
}

/*********************************************************

avinash@avinash-Vostro-1550:~/Avinash$ ls
a.out  ChangeAlphabetCase.c  EvenOrOdd.c  LsCommand.c  MyStrCan.c  mystrcat.c  mystrrev.c  PowerOfTwo.c  strcpy.c

// output using predefined ls command


avinash@avinash-Vostro-1550:~/Avinash$ vi LsCommand.c 
avinash@avinash-Vostro-1550:~/Avinash$ gcc LsCommand.c 
avinash@avinash-Vostro-1550:~/Avinash$ ./a.out

// output using user defined ls command
EvenOrOdd.c   mystrrev.c   LsCommand.c   mystrcat.c   a.out   MyStrCan.c   strcpy.c   PowerOfTwo.c   ChangeAlphabetCase.c   
avinash@avinash-Vostro-1550:~/Avinash$ 

**********************************************************/
