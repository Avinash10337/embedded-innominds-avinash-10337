/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to find out the range of signed interger

 *********************************************************************************************/

#include<stdio.h>

int main() {
	
	//declaring the local variable data
	int data=1;

	//changing the data to its maximum value
	for(int i=1;i<=30;i++) {

		data=(data<<1)|1;

	}
	
	//displaying the minimum and maximum values of an integer
	printf("range of signed integer is:%d to %d\n",~data,data);
	
	//returning from main
	return 0;

}

/*****************************************************

OUTPUT:
-------
range of signed integer is:-2147483648 to 2147483647

*****************************************************/
