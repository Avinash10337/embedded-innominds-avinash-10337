/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to check whether the given number is power of two or not.

 *********************************************************************************************/

#include<stdio.h>

int main() {

	//declaring the local variables
	int data,count=0;

	//reading the data from user
	printf("enter the data:");
	scanf("%d",&data);

	//counting the number of set bits
	for(int i=0;i<=31;i++) {
		if( (data>>i)&1 ) 
			count++;
		if(count>1)
			break;
	}

	//displaying the result whether it is power of two or not
	if(count==1) {
		printf("entered number %d is power of 2\n",data);
	}
	else {
		printf("entered number %d is not power of 2\n",data);	
	}

	//returninh from main
	return 0;

}

/***************************OUTPUT***********************

enter the data:16
entered number 16 is power of 2

enter the data:10
entered number 10 is not power of 2


**************************************************/
