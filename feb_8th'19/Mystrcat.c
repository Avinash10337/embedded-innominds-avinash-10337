/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to implement user defined string concatination function

 *********************************************************************************************/


#include<stdio.h>
#include<string.h>
void mystrcat(char[],char[]);
int main() {
	char source[100],destination[100];
	printf("enter source string: \n");
	gets(source);
	printf("enter destination string: \n");
	gets(destination);
	mystrcat(source,destination);
	printf("the destination sring after concatination is %s \n",source);
	
}

// logic to concanonate two strings
void mystrcat(char source[],char destination[])
{
	int i = 0,length=0,length1 = 0;
        length = strlen(source);
	while(destination[i]!=0)
	{
		source[length+i] = destination[i];
		i++;

	}

	source[length+i] = '\0';
}

/*******************************output**********************
 enter source string: 
innominds
enter destination string: 
Hyderabad
the destination sring after concatination is innomindsHyderabad 
*************************************************************/
