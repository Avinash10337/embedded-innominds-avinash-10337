/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
This program is to remove occurence of repeated characters

 *********************************************************************************************/


#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(){
	char arr[50]="silence is a source of great strength";
	printf("%s\n",arr);
	for(int i=0;arr[i];i++){
		char temp=arr[i];
		for(int j=i+1;arr[j];j++){
			if(arr[j]==temp){
				memmove(arr+j,arr+j+1,strlen(arr+j+1)+1);
				j--;

			}
		}
	}
	printf("%s\n",arr)}
}

/*********************************output************************

silence is a source of great strength
silenc aourfgth
***************************************************************/
