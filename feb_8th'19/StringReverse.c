/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------

Program to implementation to convert words in a string in reverse order
 *********************************************************************************************/
 

#include<stdio.h>
#include<string.h>
main() {
	char array[80],temp;
	int l,i,j,a,b;
	printf("enter the string:");
	gets(array);
	l=strlen(array)-1;
	for(i=0,j=l;i<j;i++,j--) {
		temp=array[i];
		array[i]=array[j];
		array[j]=temp;
	}

	for(i=0;array[i];i++,a=0,b=0) {
		a=i;
		while(array[i]!=32) {
			if(array[i]==0)
				break;
			b=i;
			i++;
		}
		for(;a<b;a++,b--) {
			temp=array[a];
			array[a]=array[b];
			array[b]=temp;
		}
	}
	array[i]=0;
	puts(array);
}

/***********************************************************

OUTPUT:

enter the string:Innominds Hyderabad Telengana India
India Telengana Hyderabad Innominds


***********************************************************/
