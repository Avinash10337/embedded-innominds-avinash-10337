
/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to implement user defined string copy function

 *********************************************************************************************/


#include<stdio.h>
// strcpy function declaration
void strcpy(char[],char[]);

int main()
{
	char source[100],destination[100];
	printf("enter the source string: \n");
	gets(source);
	strcpy(source,destination);
	printf("the copied string at destnation is ##%s##\n",destination);
	return 0;

}

//function to copy one string to another
void strcpy(char source[],char destination[])
{
	int i = 0;
	while(source[i]!= '\0')
	{
		destination[i] = source[i];
		i++

	}
	 
	destination[i] = '\0';

}

/*************************output***************
enter the source string: 
innominds
the copied string at destnation is ##innominds##
***********************************************/

