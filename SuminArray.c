/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
This program is to find the sum of any two elements in a matrix equal to given sum
  ----------------------------------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<stdlib.h>
int* getArray(int*,int);
int main()
{
	int array[6]={2,6,10,14,18,26};
	int n;
	int *array1=NULL;
	printf("enter the value to sum to elements in a array:\n");
	scanf("%d",&n);
	array1=getArray(array,n);
	if(array1 == NULL)
	{
		printf("the sum of any two values in an array is not equal to %d \n",n); 
	}
	else
	{
		printf("the sum equal to  %d in an array found at index at \n",n);
		for(int i=0;i<2;i++)
		{
			printf("%d ",array1[i]);
		}
		printf("\n");
	}
	return 0;
}
 

int* getArray(int *arr,int n)
{
	int i,j;
	int *arr1=(int *)malloc(2*sizeof(int));
	for(i=0,j=5;i<j;)
	{
		if((arr[i]+arr[j])==n)
		{
			arr1[0] = i;
			arr1[1] = j;
			return arr1;
		}
		else
		{
			if((arr[i]+arr[j])>n)
			{
				j--;
			}
			else
			{
				i++;
			}
		
		}
	}
	return NULL;
}
/******************output******************
 
enter the value to sum to elements in a array:
12
the sum equal to  12 in an array found at index at 
0 2 

**************************************************/
  
