/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
This program is to reverse the file contents using fseek.

 *********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp1=NULL,*fp2=NULL;
	int ch,len;
	//opening input file to read
	if((fp1 = fopen("input.txt","r")) == NULL) {
		printf("unable to open the file\n");
		return -1;
	}
	else
		printf("INPUT FILE HAS  OPENED SUCCESSFULLY\n");
	//opening outputfile to write
	if((fp2 = fopen("Output.txt","w")) == NULL) {
		printf("unable to open the output file\n");
		return -1;
	}
	else
		printf("OUTPUT FILE HAS OPENED SUCCESSFULLY\n");
	printf("------------------------------------\n");
	printf("printing the contents of input file \n");
	while((ch = fgetc(fp1)) != EOF){
		printf("%c",ch);
	}
	printf("------------------------------------\n");
	fseek(fp1,-1,SEEK_END);
	len = ftell(fp1);
	printf("the size of input file is  %d\n",len);
	while(len >= 0){
		ch = fgetc(fp1);
		fputc(ch,fp2);
		fseek(fp1,-2,SEEK_CUR);
		len--;
	}
	fclose(fp2);
	fp2 = fopen("Output.txt","r");
	printf("------------------------------------\n");
	printf("printing the contents of output file \n");
	while((ch = fgetc(fp2)) != EOF){
		printf("%c",ch);
	}
	printf("\n");
	printf("------------------------------------\n");
	//closing the files 
	fclose(fp1);
	fclose(fp2);
	return 0;
}

/***********************OUTPUT**************************************************************************************
INPUT FILE HAS  OPENED SUCCESSFULLY
OUTPUT FILE HAS OPENED SUCCESSFULLY
------------------------------------
printing the contents of input file 
Our customer is a leading consulting services provider who was looking for a product engineering and solutions partner who could facilitate building analytics solutions in the areas of consumer and retail industries.
One such customer scenario was related to e-commerce reverse auction domain.

------------------------------------
the size of input file is  294
------------------------------------
printing the contents of output file 


.niamod noitcua esrever ecremmoc-e ot detaler saw oiranecs remotsuc hcus enO
.seirtsudni liater dna remusnoc fo saera eht ni snoitulos scitylana gnidliub etatilicaf dluoc ohw rentrap snoitulos dna gnireenigne tcudorp a rof gnikool saw ohw redivorp secivres gnitlusnoc gnidael a si remotsuc ruO

*****************************************************************/

