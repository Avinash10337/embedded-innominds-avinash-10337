/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to implement operations like setBit,clear,compliment,toggleBit,clearing the bits upto a specified range given by the user

 *********************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

int setBit(int num,int pos){
	int result = 0;
	result = num | (1<<pos);
	printf("the result after setting bit at %d is %d \n",pos,result);
	displayInBinary(result);
}


int clearBit(int num,int pos){
	int result = 0;
	result = num & (~(1<<pos));
	printf("the result after clearing bit at %d is %d \n",pos,result);
	displayInBinary(result);
}


int complimentBit(int num,int pos){
	int result = 0;
	result = num ^ (1<<pos);
	printf("the result after complimenting bit at  %d is %d\n",pos,result);
	displayInBinary(result);
}


void toggleBit(int num){
	int result = 0;
	result = ~num ;
	printf("the result after setting bit is %d \n",result);
	displayInBinary(result);
}

void displayInBinary(int num){
	int i,result;
	for(i=31;i>=0;i--){
	printf("%d",1 & (num>>i));
	}
}

void clearRangeOfBits(int num,int pos) {
	int result = 0;
	for(int i=0;i<=pos;i++) {
	num = num & (~(1<<i));
	}
	printf("the result after clearing bit from 0 to %d is %d \n",pos,num);
	displayInBinary(num);
}


void checkBitStatus(int num,int pos){
	if((num >> pos)&1)
	{
		printf("bit at given position %d is 1\n",pos);
	}
	else
               {
		printf("bit at given position %d is 0\n",pos);
	      }
}

void countSetBits(int num){
	int count =0;
	for(int i=0;i<=31;i++){
		if((num>>i)&1){
			count++;
		}
	}
printf("the set bits in given number is %d\n",count);
}


  
int main() {
	int num,pos,choice,i;
	printf("####################################################################\n");
	printf("############# BIT MANIPULATION #####################################\n");
	printf("####################################################################\n");
	printf("enter the number\n");
	scanf("%d",&num);

	while(1){
	printf("\n--------------------------------------------\n");
	printf("\n1)set bit\n2)clear bit\n3)compliment bit\n4)toggleBit\n5)displayInBinary\n6)clear range of bits\n7)count set bits\n8)check bit status\n9)exit\n");
       printf("enter your choice\n");
	scanf("%d",&choice);
	switch(choice){
		case 1:
			printf("enter the position at which you want to set the bit:");
			scanf("%d",&pos); 
			setBit(num,pos);
			break;

		case 2: 
			printf("enter the position at which you want to  the bit:");
			scanf("%d",&pos); 
			clearBit(num,pos);
			break;

		case 3: 
			printf("enter the position at which you want to complement the bit:");
			scanf("%d",&pos); 			
			complimentBit(num,pos);
			break;

		case 4: toggleBit(num);
			break;

		case 5: displayInBinary(num);
			break;
		case 6: 
			printf("enter the position upto where you want to clear the bit:");
			scanf("%d",&pos); 		
			clearRangeOfBits(num,pos);
			break;
		case 7: countSetBits(num);
			break;
                      
                case 8: 
			printf("enter the position upto where you want to clear the bit:");
			scanf("%d",&pos); 		
			checkBitStatus(num,pos);
			break;
			
		default: printf("enter valid choice\n");


	 }


}}

/*************************************OUTPUT*********************************************
 * ####################################################################
############# BIT MANIPULATION #####################################
####################################################################
enter the number
2

--------------------------------------------

1)set bit
2)clear bit
3)compliment bit
4)toggleBit
5)displayInBinary
6)clear range of bits
7)count set bits
8)check bit status
9)exit
enter your choice
5
00000000000000000000000000000010
--------------------------------------------

1)set bit
2)clear bit
3)compliment bit
4)toggleBit
5)displayInBinary
6)clear range of bits
7)count set bits
8)check bit status
9)exit
enter your choice
1
enter the position at which you want to set the bit:2
the result after setting bit at 2 is 6
00000000000000000000000000000110
--------------------------------------------

1)set bit
2)clear bit
3)compliment bit
4)toggleBit
5)displayInBinary
6)clear range of bits
7)count set bits
8)check bit status
9)exit
enter your choice
3
enter the position at which you want to complement the bit:2
the result after complimenting bit at  2 is 6
00000000000000000000000000000110
--------------------------------------------

1)set bit
2)clear bit
3)compliment bit
4)toggleBit
5)displayInBinary
6)clear range of bits
7)count set bits
8)check bit status
9)exit
enter your choice
1
enter the position at which you want to set the bit:1
the result after setting bit at 1 is 2
00000000000000000000000000000010
--------------------------------------------

1)set bit
2)clear bit
3)compliment bit
4)toggleBit
5)displayInBinary
6)clear range of bits
7)count set bits
8)check bit status
9)exit
enter your choice
6
enter the position upto where you want to clear the bit:1
the result after clearing bit from 0 to 1 is 0
00000000000000000000000000000000
--------------------------------------------

1)set bit
2)clear bit
3)compliment bit
4)toggleBit
5)displayInBinary
6)clear range of bits
7)count set bits
8)check bit status
9)exit
enter your choice
7
the set bits in given number is 1

--------------------------------------------

1)set bit
2)clear bit
3)compliment bit
4)toggleBit
5)displayInBinary
6)clear range of bits
7)count set bits
8)check bit status
9)exit
enter your choice
8
enter the position upto where you want to clear the bit:2
bit at given position 2 is 0

--------------------------------------------

1)set bit
2)clear bit
3)compliment bit
4)toggleBit
5)displayInBinary
6)clear range of bits
7)count set bits
8)check bit status
9)exit
enter your choice


 


***************************************************************************************/
