/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
This program is to implement swap operation using xor.
*********************************************************************************************/
#include<stdio.h>

int main() {
	int data1,data2;
	printf("enter data1 and data2:");
	scanf("%d%d",&data1,&data2);
	printf("before swap:data1=%d data2=%d\n",data1,data2);
	data1^=data2^=data1^=data2;
	printf("after swap:data1=%d data2=%d\n",data1,data2);
}

/**********************OUTPUT************************
 enter data1 and data2:89
56
before swap:data1=89 data2=56
after swap:data1=56 data2=89
****************************************************/
