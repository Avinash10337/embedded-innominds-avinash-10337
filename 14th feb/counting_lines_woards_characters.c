#include<stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp=NULL;
	int ch;
	int line_Count=0,ch_Count=0,word_Count=0;
	if((fp=fopen("count.txt","r")) == NULL) {
		printf("Failed to open the file........\n");
		return -1;
	}
	//opening the file
	printf("FILE HAS OPENED SUCCESSFULLY\n");
	while((ch=fgetc(fp))!=-1) {
		if(ch == ' ')
			word_Count+=1;
		else if(ch == '\n')
			line_Count+=1;
		else
			ch_Count+=1;
	}
	printf("Total Number of charecters in the file are :%d\n",ch_Count);
	printf("Total Number of words in the file are :%d\n",word_Count+line_Count);
	printf("Total Number of lines in the file are :%d\n",line_Count);
	//closing the fle
	fclose(fp);
	return 0;
}

/*******************************OUTPUT************************************

FILE HAS OPENED SUCCESSFULLY
Total Number of charecters in the file are :89
Total Number of words in the file are :17
Total Number of lines in the file are :3
***********************************************************************/
