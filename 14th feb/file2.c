/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
This program is to merge the content of two files into third file

 *********************************************************************************************/


#include<stdio.h>
#include<stdlib.h>

int main() {
	char ch;
        
	FILE *fp1 =NULL,*fp2=NULL,*fp3 = NULL;
        // opening the files
	fp1 = fopen("test1.txt","r");
	fp2 = fopen("test2.txt","r");
	fp3 = fopen("test3.txt","w");
        // checking if the content in the file is present or not
	if(fp1 == NULL || fp2 == NULL || fp3 == NULL) {
		printf("cannot open the file\n");
		exit(0);
	}
        // writing the content of fp1 file to fp3
	while((ch = fgetc(fp1))!= EOF)
		fputc(ch,fp3);
        // writing the content of fp2 file to fp3
	while((ch = fgetc(fp2))!= EOF)
		fputc(ch,fp3);
//	fprintf(fp3,"%c",ch);

	fclose(fp1);
	fclose(fp2);
	fclose(fp3);
	return 0;

}

/************************* OUTPUT *******************************

*/
