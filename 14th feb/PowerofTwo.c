/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
In this program we are checking the given integer is even or odd

**********************************************************************************************/

#include<stdio.h>

int main() {
	int data,cnt=0;
	printf("enter data:");
	scanf("%d",&data);
	for(int i=0;i<=31;i++) {
		if((1<<i) & data)
			cnt++;
	}
	if(cnt==1)
		printf("entered number %d is power of 2\n",data);
	else
		printf("entered number %d is not power of 2\n",data);
	return 0;
}

/*******************OUTPUT***************************
 
enter data:6
entered number 6 is not power of 2
------------------------------------------------
enter data:32
entered number 32 is power of 2

****************************************************/
