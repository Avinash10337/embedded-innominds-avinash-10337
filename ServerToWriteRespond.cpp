
/*---------------------------------------------------------------------------------------------------------------------------------*/
/*                 NAME:                 EMPLOYEE-ID:           PHONE_NUMBER:                          GMAIL-ID:
		V.V.AVINASH          10337                   7675057184                    v.v.avinash7777@gmail.com
 
  PURPOSE
-----------

 THIS PROGRAM WORKS AS SERVER WHICH READS THE DATA FROM THE SHARED MEMORY which is sent by the client and respond accordingly
------------------------------------------------------------------------------------------------------------------------------------*/

#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <unistd.h>  
#include <stdio.h> 
using namespace std; 
//Server class has data members and member function
class ServerToWriteRespond {
	
	int shmid ;
	public :
	//default constructor
	ServerToWriteRespond() {
		
		key_t key = ftok("myfile1",85);
		shmid= shmget(key,1024,0666|IPC_CREAT); 


		char *str = (char *)shmat(shmid,(void*)0,0);  

		gets(str);
		shmdt(str);
	}
	//print_Message will read the data from the shared memory and display the data
	void print_Message() {

		char *str1 = (char *)shmat(shmid,(void*)0,0);  
		cout<<"WELCOME......"<< str1<<endl;
	}
};




int main() 
{ 
	int i=0;
	//object is created for the class Server
	ServerToWriteRespond serverObj;
		while(i<3) {
		i++;
		sleep(20);
		serverObj.print_Message();
	}
	return 0; 
}




/**************************************************************************************************
 
     OUTPUT
  ----------------
hello
WELCOME......keerthana
WELCOME......avinash
WELCOME......prasanna

********************************************************************************************************/ 
