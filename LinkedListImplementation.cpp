
/*************************************************************************************************
 
 NAME:                  EMPLOYEE ID:      MOBILE NO:           EMAIL ID:

 V.V.AVINASH            10337             7675057184           v.v.avinash7777@gmail.com



 Purpose
----------
 This program is the implementation on Linked List,which performs the operations of adding,updating and 
 deleting  data at a specified position, and also displaying data present in the Linked List.
   
*************************************************************************************************/



#include<iostream>
#include<stdio.h>
#include<stdlib.h>

using namespace std;

// creating a class LinkedListImplementation which contains structure and member function
class LinkedListImplementation {
	int no_of_records;
	struct List {
		string name;
		struct List *next;
	}*head;
	public:
        // parameterised constructor 
	LinkedListImplementation(string name) {
		head=new struct List;
		head->name=name;
		head->next=NULL;
		no_of_records=1;
	} 
   
        // method to add data at a particular postion 	
	void addAtPosition(string name,int position) {

		struct List *new_record=new struct List;
		new_record->name=name;
		if (position <0 || position > no_of_records+1) {
			cout<< "cant add the record at this position:"<<position<<endl;
                        cout<< "Enter a valid Position"<<endl;
		}
		else if(position==0) {
			new_record->next=head;
			head=new_record;
			no_of_records++;
			return;
		}
		else if(position==no_of_records+1) {
			struct List *temp;
			if(head==NULL) {
				head=new_record;
				no_of_records++;
				return;
			}
			for(temp=head;temp->next!=NULL;temp=temp->next);
			temp->next=new_record;
			new_record->next=NULL;
			no_of_records++;
			return;
		}
		else {
			int index;
			struct List *current_record,*previous_record;
			current_record=head;
			for(index=1;index!=position+1;index++) {
				previous_record=current_record;
				current_record=current_record->next;
			}
			previous_record->next=new_record;
			new_record->next=current_record;
			no_of_records++;
			return;
		}
	}

         // This method is used to display records present in the list
	void displayRecords() {
		if(head==NULL) {
			cout<<"List is empty."<<endl;
			return;
		}
		struct List *temp;
		cout<<"Total no.of records:"<<no_of_records<<endl;
		cout<<"\t**********LIST*********"<<endl;
		for(temp = head;temp!=NULL;temp = temp->next) {
			cout<<"\t\t"<<temp->name<<endl;
		}
		delete temp;
		return;
	}

        // This method is used to delete a record at particular position 
	void deleteAtPosition(int position) {

		if (position <=0 || position > no_of_records) {
			cout<< "cannot delete the record at that position"<<position<<endl;
                        cout<<"enter valid position"<<endl;
		}
		else if(position==1) {

		struct List *temp=head;
		head=head->next;
		delete temp;
		temp=NULL;
		no_of_records--;
		return;
		//	deleteFromBeginning();
			return;
		}
		else if(position==no_of_records) {
		//	deleteFromEnding();
		struct List *temp,*previous;
		for(temp=head;temp->next!=NULL;previous=temp,temp=temp->next);
		previous->next=NULL;
		delete temp;
		temp=NULL;
		no_of_records--;
			return;
		}
		else {
			int index;
			struct List *current_record,*previous_record;
			current_record=head;
			for(index=1;index!=position;index++) {
				previous_record=current_record;
				current_record=current_record->next;
			}
			previous_record->next=current_record->next;
			delete current_record;
			current_record=NULL;
			no_of_records--;
			return;
		}
	}

 // This method is used to update the data at a specified Position
 void updateAtPosition(int position) {
                if(position<0 || position > no_of_records) {
                        cout<<"cannot update the data at that position:"<<position<<endl;
                        cout<<"enter a valid position"<<endl;
                }
                int index;
              struct List *temp=head;
                for(index=1;index<position;index++) {
                        temp=temp->next;
                }
                cout<<"enter the name to be update:"<<endl;
                cin>>temp->name;
              }

 // Destructor to release the memory
  	~LinkedListImplementation() {
		delete head;
	}
};

// main method which uses switch case to call the member function
int main(void) {
	LinkedListImplementation linkedListObj("Avinash");
	string name;
	int choice;
	while(1) {
                cout<<"-----------------------------------------------------------------"<<endl;
                cout<<"\t\t\t Linked List OPerations "<<endl;
		cout<<"*******************MENU*******************"<<endl;
		cout<<"1.Add record at given position\n2.Delete at given position\n3.Update at a particular postion\n4.Display all records\n5.exit"<<endl;
                cout<<"_________________________________________________________________________"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
				{
					cout<<"Enter name to be add into list:"<<endl;
					cin>>name;
					int position;
					cout<<"Enter the position at which u want to add:"<<endl;
					cin>>position;
					linkedListObj.addAtPosition(name,position);
				}
				break;
			case 2:
				{
					int position;
					cout<<"Enter the position:"<<endl;
					cin>>position;
				    	linkedListObj.deleteAtPosition(position);
				}
				break;

                        case 3:
                               
                                {
                                        int position;
                                        cout<<"Enter the position:"<<endl;
                                        cin>>position;
                                        linkedListObj.updateAtPosition(position);
                                }
                                break;

			case 4:
				linkedListObj.displayRecords();
				break;
			case 5:
				exit(0);
			default: 
				cout<<"Invalid option"<<endl;
                                cout<<"Enter choice between (1-5)"<<endl;

		}
	} 


}



/********************************************************************************

 OUTPUT
---------
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:
1
Enter name to be add into list:
Innominds
Enter the position at which u want to add:
1
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:
1
Enter name to be add into list:
Hyderabad
Enter the position at which u want to add:
2
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:
4
Total no.of records:3
	**********LIST*********
		Avinash
		Innominds
		Hyderabad
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:
2
Enter the position:
3
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:
4
Total no.of records:2
	**********LIST*********
		Avinash
		Innominds
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:
1
Enter name to be add into list:
Hyderabad
Enter the position at which u want to add:
3
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:
3
Enter the position:
3
enter the name to be update:
Welcome
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:
4
Total no.of records:3
	**********LIST*********
		Avinash
		Innominds
		Welcome
-----------------------------------------------------------------
			 Linked List OPerations 
*******************MENU*******************
1.Add record at given position
2.Delete at given position
3.Update at a particular postion
4.Display all records
5.exit
_________________________________________________________________________
enter your choice:


*****************************************************************************/



