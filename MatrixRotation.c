/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to rotate the matrix in 90 degrees and display it.

 *********************************************************************************************/


#include<stdio.h>
#include<stdlib.h>
//it displays the elements in the matrix
void printMatrix(int rows,int columns,int matrix[][columns]) {

	for(int i=0;i<rows;i++) {
		for(int j=0;j<columns;j++) {
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}
}
// main function
int main() {
	int rows,columns;
	int i,j;
 
        // taking no.of rows in a matrix from user      
	printf("enter how many rows in a matrix \n");
	scanf("%d",&rows);
	while(rows<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&rows);
	}
        // taking no.of columns in a matrix from user
	printf("enter how many columns in a matrix \n");
	scanf("%d",&columns);
	while(columns<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&columns);
	}
	int a[rows][columns];
	int b[columns][rows];
        // entering the elements in a matrix
	printf("enter the elements into the matrix\n");
	for(i=0;i<rows;i++) {
		for(j=0;j<columns;j++) {
			scanf("%d",&a[i][j]);
		}
	}
	printf("The elements in the matrix are \n");

	printMatrix(rows,columns,a);

	for(j=0;j<columns;j++) {
		for(i=0;i<rows;i++) {
			b[j][i]=a[i][j];
		}
		printf("\n");
	}

	int temp;

	for(i=0;i<columns;i++) {
		for(j=0;j<rows/2;j++) {
			temp = b[i][j];
			b[i][j]=b[i][rows-1-j];
			b[i][rows-1-j]=temp;
		}
	}
	printf("the matrix after 90 degree rotation is dispayed below \n");
	printMatrix(columns,rows,b);
}
/*****************output***************************
 
enter how many rows in a matrix 
3
enter how many columns in a matrix 
3
enter the elements into the matrix
1
3
4
2
5
6
78
8
9
The elements in the matrix are 
1 3 4 
2 5 6 
78 8 9 



the matrix after 90 degree rotation is dispayed below 
78 2 1 
8 5 3 
9 6 4 
******************************************************/:
