/*********************************************************************************************
NAME:         |         EMP ID:   |   EMAIL ID:                    |             PHONE:      |
---------     |      ------------ |  -----------------             |         --------------  | 
V.V. AVINASH  |         10337     |   v.v.avinash7777@gmail.com    |           7675057184    |

purpose:
---------------
Program to implement the all types of add and delete operations of a singly linked list.

 *********************************************************************************************/

// header files inclusion
#include<stdio.h>
#include<stdlib.h>

// functions declarations
void addAtAfter();
void addAtEnd();
void addAtMiddle();
void deleteAtBeginning();
void deleteAtAfter();
void deleteAtEnd();
int  length();
void display();

// creating a structure called node with data and link part
struct node
{
	int data;
	struct node *link;
};

struct node *root = NULL;


// to find the length of list
int length()
{
	int count=0;
	struct node *temp;
	temp = root;
	while(temp!= NULL)
	{
		count++;
		temp = temp->link;
	}
	printf(" the length of current list is %d\n",count);
	return count;
}


// add node at beginning of list.
void addAtBeginning()
{
	struct node *temp;
	temp = (struct node*)malloc(sizeof(struct node));
	printf("enter node data\n");
	scanf("%d",&temp->data);
	temp->link = NULL;
	if(root == NULL)
	{
		root = temp;
	}
	else
	{
		temp->link = root;
		root =temp;
	}
}

// add node at a node after where user specify the position
void addAtAfter()
{
	struct node *temp,*p;
	int location,len,i = 1;
	printf(" enter location \n");
	scanf("%d",&location);
	len = length();
	if(location > len && location <0)
	{
		printf("Invalid Location\n");
		printf(" currently list is having %d nodes",len);
	}
	else
	{
		p = root;
		while(i<location)
		{
			p = p->link;
			i++;
		}

		temp = (struct node*) malloc(sizeof(struct node));
		printf("enter node data\n");
		scanf("%d",&temp->data);
		temp -> link = NULL;
		temp -> link = p->link;
		p->link = temp;
	}
}

// add node at end of linked list
void addAtEnd()
{
	struct node *temp;
	temp = ((struct node*)malloc(sizeof(struct node)));
	printf("enter node data\n");
	scanf("%d",&temp -> data);
	temp -> link = NULL;
	struct node *p;
	p = root;
	while(p -> link!= NULL)
	{
		p = p-> link;
	}
	p->link = temp;
}

// displaying data present in all nodes
void display()
{
	struct node *temp;
	temp = root;
	if(temp == NULL)
	{
		printf(" no nodes present in the list\n");
	}
	else
	{
		while(temp!= NULL)
		{
			printf("-> %d",temp->data);
			temp = temp->link;
		}
	}
}

// deleting a node at the beginning of linked list
void deleteAtBeginning()
{
	struct node *temp;
	temp = root;
	root = temp->link;
	temp->link = NULL;
	free(temp);
}

// delete a node at a user specify position
void deleteAtAfter()
{
	struct node *q,*p = root;
	int i=1,location;
	printf("enter location to be delete\n");
	scanf("%d",&location);
	if(location > length() && location <0)
	{
		printf("Invalid location\n");
	}
	else
	{
		while(i < location-1)
		{
			p = p->link;
			i++;
		}
		q = p->link;
		p->link = q->link;
		q->link = NULL;
		free(q);
	}
}

// delete a node at the end of linked list
void deleteAtEnd()
{

	struct node *q,*p = root;
	int i=1,location;
	printf("enter location to be delete\n");
	scanf("%d",&location);
	if(location > length() && location <0)
	{
		printf("Invalid location\n");
	}
	else
	{
		while(i < location-1)
		{
			p = p->link;
			i++;
		}
		q = p->link;
		p->link = q->link;
		q->link = NULL;
		free(q);
	}
}

// main function
int main()
{

	int choice;
	while(1)
	{
		printf("\n##########################################################\n");
		printf("###### SINGLE LINKED LIST OPERATIONS #####################\n");
		printf("##########################################################\n");
		printf("1) AddatBeginning\n");
		printf("2) AddatAfter\n");
		printf("3) AddatEnd\n");
		printf("4) DeleteatBeginning\n");
		printf("5) DeleteatAfter\n");
		printf("6) DeleteatEnd\n");
		printf("7) Length\n");
		printf("8) Display\n");
                printf("------------------------------------------------------\n");
		printf("Enter your choice\n");
		scanf("%d",&choice);
                // using switch case where we call functions
		switch(choice)
		{

			case 1: addAtBeginning();
				break;
			case 2: addAtAfter();
				break;
			case 3: addAtEnd();
				break;
			case 4: deleteAtBeginning();
				break;
			case 5: deleteAtAfter();
				break;
			case 6: deleteAtEnd();
				break;
			case 7: length();
				break;
			case 8: display();
				break;
			default : printf("enter valid choice\n");
				  break;
		}

	}         

	return 0;
}







/**************************************************************************************

   OUTPUT:
 -----------
 ##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
8
 no nodes present in the list

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
8
 no nodes present in the list

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
^Z
[9]+  Stopped                 ./a.out
avinash@avinash-VirtualBox:~/aspire$ cc linkedlist.c 
avinash@avinash-VirtualBox:~/aspire$ ./a.out

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
7
 the length of current list is 0

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
8
 no nodes present in the list

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
1
enter node data
10

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
2
 enter location 
1
 the length of current list is 1
enter node data
20

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
3
enter node data
30

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
4

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
8    
-> 20-> 30
##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
5
enter location to be delete
1
 the length of current list is 2

##########################################################
###### SINGLE LINKED LIST OPERATIONS #####################
##########################################################
1) AddatBeginning
2) AddatAfter
3) AddatEnd
4) DeleteatBeginning
5) DeleteatAfter
6) DeleteatEnd
7) Length
8) Display
------------------------------------------------------
Enter your choice
8
*/
